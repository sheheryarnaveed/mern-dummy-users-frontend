# Setup
1. `git clone https://gitlab.com/sheheryarnaveed/mern-dummy-users-frontend.git`
2. `cd` into the cloned directory
3. `npm install`
4. `npm start`


## Note
- Frontend is developed in ReactJS
- Make sure to start the backend before running the frontend
- The frontend runs on port 3000



