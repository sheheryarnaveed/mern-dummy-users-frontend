import React from 'react';
import './App.css';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

toast.configure();

class UserPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
   this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    this.loadUsers();
  }  


  loadUsers() {

    fetch("http://localhost:3001/api/users",{credentials: "include"}) //setting credentials to true to allow cookie set
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            users: result
          });
          toast.success("Profiles Loaded Successfully!", {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 3000
          });
        },
        (error) => {
          console.log(error)
          toast.error("Error Occured: "+error.toString() , {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 5000
          });
        }
      )
    
  }

  handleDelete(e){
    e.preventDefault(e);
    var id = e.target.rel;
    var confirmation = window.confirm('Are you sure you want to delete the user with id "' + id + '" ?');
    if(confirmation === true && id){

      fetch("http://localhost:3001/api/users/"+id, {method: 'DELETE', credentials: "include"}) //setting credentials to true to allow cookie set
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
          if (result.msg === ""){
            
            //Instead of recalling the loadusers function, do this to avoid additional API call network cost
            var refinedUsers = this.state.users.filter(function(value, index, arr){ 
              return value.id !== id;
            });
            this.setState({
              users: refinedUsers
            });
            toast.success("Deleted Successfully!", {
              position: toast.POSITION.TOP_RIGHT,
              autoClose: 3000
            });

          }
          else{
            console.log("Deletion Failed: " + result.msg);
            toast.warn("Deletion Failed. Possible Causes: Backend server problem!", {
              position: toast.POSITION.TOP_RIGHT,
              autoClose: 5000
            });
          }
        },
        (error) => {
          console.log(error)
          toast.error("Error Occured: "+error.toString() , {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 5000
          });
        }
      )

      
    }
  }


	
  render() {
    return (
      <div id="wrapper">
        <h1 className="header"> Users Base</h1>
        <UserList 
          users={this.state.users}
          handleDelete={this.handleDelete}
        />
      </div>
    );
  }
}


class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleDelete(e){
    e.preventDefault(e);
    this.props.handleDelete(e);
  }

  render() {
    let rows = [];
    Object.keys(this.props.users).map((user, index) => {
        rows.push(
          <UserRow
            index={index}
            user={this.props.users[index]}
            handleDelete={this.handleDelete}
          />
        );
    });

    return (      
      <div id="userList">
      <table id="users">
        <thead>
          <tr>
            <th>#</th>
            <th>ID</th>
            <th>Title</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
      </div>
    );
  }
}

class UserRow extends React.Component {
  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleDelete(e){
    e.preventDefault(e);
    this.props.handleDelete(e);
  }
  
 render() {
    const user = this.props.user;
    const key = this.props.index +1;
    return (
      <tr>
        <td><p>{key}</p></td>  
        <td><a href="" onClick={this.handleDelete} rel={user.id}>{user.id}</a></td>  
        <td><p>{user.title}</p></td>  
        <td><p>{user.firstName}</p></td>  
        <td><p>{user.lastName}</p></td>  
        <td><p>{user.email}</p></td>  
        <td><div><img src={user.picture} width="60" height="60" /></div></td>     
      </tr>
    );
  }
}

export default UserPage;
